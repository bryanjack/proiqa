import {View, Text, StyleSheet, Button, Alert} from 'react-native';
import React from 'react';
import {NativeModules} from 'react-native';

const App = () => {
  const showLiveness = () => {
    // Access Key, should be filled using client's accessKey & secretKey
    // And also application id should be registered in dashboard
    // const accessKey = 'xxxxxx';
    // const secretKey = 'xxxxxx';
    const lic = "jpgVjAcYGzmzWmhtJwSVwtdAex47BxrIlEy9saZDqtcmlgOFOugfyTD/0u/WgxNFBDGyyemAbt3PSIOqnxqw2eMFE0khrRr4du6IA+whXoy2axi3X7L91mRjIGj5o70x9X7oh6Hgb5f7CzZBQFXWUNa12Y4aCTlPz9B0OY0LI5PxfOZ4+ep7XJ6HJrBCuMZnu2C+VjecjHyn/n3EAqJ1RQ1BAvavFw88tmq9uBkEGf5FgD7ySrpoqZRJR3cSr2c5PhElX7+6Bs5mdLcjdLFgW5IOgluZYPEjTMHy8nkJpRKHxlJKokG+GB2CsFpPhpUNwkp11ZV891Z76d+SQE0bcHLZrMHOdV2lE+JF/PEA8fK6pG9QCKJOibMnk4VsobsfAayIQmaN7ftQ/cgND75tnRXSfDGRuvguqQW9s1oAMq0Z465mzHSnrGo5sACmR1OiYpHvxhBYuBQLchXiou5zCPE3OLZ8aEuuHWVgECjVBO4iD0nw7mcqeSb0NeqBHzmxLJlZLnD+MsFmkomwZA6PotqxkJSiAcuPX58XWGeGTVL4a0SEZPQx5iiZckZV+ii1DTFvFjfKVsFu6t0erMQU9kt4Ulii+aEx1rvbu+VWif3tOKPpZ24hWuGrF8c/jZwAAWetnTec7L86S542V13ILoLvfDCHtLG26cVwgGOnP4Rc38R3pBg7KLeKUzU8Um/5pq4RGLb6gMkaFhAgQf6hPT0e7FcW/3DR1OioMttK1HzJd9R4IPvsx+wtwL/bttIALb6jhpnjLqgBRPWMrIwIxIbvIQPqom/H5lgh/2VRe0lm7qu3TI1dcfX2S6uMdW0WQac4+R0QGXcvWZrrmQX34TYxiQEqfwy74cBo+WvtJYASyvhFjkx/vQgBTmQ4z/WKHQPWXACWdnv/QT+yxLUnsLjOP6yXEpjkcR9xSQJpW/yAdGvsG8Kbme9dpESJolTgosYrdJMPjOa7qBg8A/rYThkWRq/HlnFRJTp5QadJuqhYWksA4ugs+zX7B2Wbv9FrG0t/Pml/TvXc5LzdG4Cjr5pM8EY/8YMFXDgC7HYk8eTAIwSTxM9JVno47aq1uGKaqeDicAYe5H+TZZvYNlh82k/1/RmOpNMyQghLKWbT/QC9tvFewGURUnV2RYTrPZn+30v7IfE8bsi3pObJPlB/lIuv1E3+gsIuoQJEHYB7pJ5VSPY+V0sZ6e2DwE4Qq3ckYT7w2x2vYeVSjhMfjKhox366iUFXMi9uO00wSvB2xp7A9HTtHnowNJbu2EHIdq9DAYjWMwQ4Ej9ZxRgNxakjJiLV49hi2wnPT9ANO0FzSZQ/wAYaOYZvanhwd5hGSfNQn3Hi8RFvt5MZa0CKRDwSPqjlT9QPy5I8mL5TC1ieEU7dpSPTOaUDkfJECmwvJRQge1rMmkqEfzviGIWNXdw+TWTsUTVYSULQUPnvGvBeRfGUqB+cr7UIsGtXIzTDwlynQRcnmqtrS0EJWi+/xIZ2QSkVuAx8ACrGQ1MblOqkb90SmAAqti+JLvMbeh0rAmgnrQ3wUYPrX9EiqGuWkDmKuQL/tJCOLrSsjsIaT5aVyBpJXtZPiV1bqhJ/h2dEj6ZUDlzwMhXLI/KyKYThJSJlCtsIrMaA6500gv88Wr42DcQTaW9igYoTHllRfVfm/Ri7hFQld/uRoBC6yoQkE9NgtwrwD1ZNXFR9EZYXQuvtVcApqiph0kGS/sqHydot7yUOOVbUUwpDKa5ONuz4Xef1+xyaibxndbqIvwQNexh9ewzgM1UB4I4vRB/lGS+9/ExkaoJyFJPLh1UiCXoXjqsf/GyzuMjF+dEEn5XwHKsY0KcFx2ZsJSO307fkgitnly7RnggIruiyAW0ySyd1NoE6wBs3WeQmXC2Y2UfALWO7hZ3deoPpo6zrq7AJYv9wdyXX/Ph0wT3lbbmLiinCAIzwhmofM5gv/NV5t+XRyFBN4gGUJFv6T2YhmpmvDlbc7+4Gi0hLtAhMBomN/mBZfxF99VSGmwLCtCZ7l+9mH+aUGDyBTq5dFCBBfcGLWBw0MVMpczdxwk20Tnp5L/8iNdtDO/Qj90rM+rAVO+o+R0WZkmk1Ai8koX+ifwPwYe/+zxGXQz+80BN9tXhvVHht//LPA9hsEFHtyVzDvC91nNnm5sxVbT7q3eQQA7TXF0DWgYt1l/Oxou3UG5qYlAlx+lB1Fa44FltuH3AJpFT5XXHOlHSgiRuZITRgEgvWtGWk5U/vJIoDzo4uf90biD3LDAazcV3Yp1IGIft6VPYmoy7dxj/CYC+zp1X70ahKFOkiP3XwqHzzxxwh+Ajw16Y7TARPBxDDPJF8k2imKn2wj1Q4SL6AA2tIriAMCIjyMLu5sWVwEZZwx7Xm4hd4p7FjVifxC92wS8M+Lc0hJj/pxNJZFjnrdASBA5O7zmVQiPWE2JDcpudkYxmAjoPTzKWj3cznuJ1jG8sKP48hvdGmK7vO6AV5sKJ7soO6YxDhCzJjpM5RaUZNxGwaDyhW+87GIU2pqYfiJf9Gtkpdz3fDYA8yYXVV33JSc0+iCmPQIUubndo49Chm42DrS4YxFoCTGzmNzjRhqDp7VXt0QwJd8w/bJU7Op75B+sx2vJFWa1MWMC8mxLIrWmumI5jXqbCzB0nJEi9oHdiQ7fVNQw6dZoyonPv+farWJDeX1nv/PQ==";

    // NativeModules.LivenessModule.initSDKByKey(
    //   accessKey,
    //   secretKey,
    //   'Indonesia',
    //   false,
    // );

    // IQA
    NativeModules.GlobalIQAModule.initSDK()

    // Check License
    NativeModules.GlobalIQAModule.setLicenseAndCheck(lic,(code)=>{
      // License check pass
      console.log("license success")
      },(errorCode)=>{
      // Error license
      console.log("license failed")
      })

      // User Binding
      NativeModules.GlobalIQAModule.bindUser('08128000')


      // After the detection is over, we will return the detection results immediately

      const extras = { region: "ID", cardType: "ID_CARD",cardSide:"FRONT"}

      NativeModules.GlobalIQAModule.startGlobalIQA(extras,result=>{
            console.log(JSON.stringify(result));
            Alert.alert('Success', 'idvid : '+JSON.stringify(result.idvid)+'\nTrxId : '+JSON.stringify(result.transactionId));
      },(errorCode)=>{
        // Error license
        console.log(errorCode)
        })

      
      

    // NativeModules.LivenessModule.initSDKByLicense("Indonesia",false);

    // NativeModules.LivenessModule.setLicenseAndCheck(lic,(successCode)=>{
    //   // license check success,you can start liveness detection.
    //   NativeModules.LivenessModule.startLiveness(
    //     (successJsonData)=>{
    //       // This callback indicates Liveness Detection success
    //       Alert.alert('success')
    //     },
    //     (failedJsonData)=>{
    //     // This callback indicates that the Liveness Detection failed or the user canceled the detection
    //       Alert.alert('failed')
    //     }
    //   )

    //  },(errorCode)=>{
    //      // license is not available, expired/wrong format/appId not match
    //      Alert.alert(errorCode)
    //      Alert.alert('Error code')
    //  })


    // the first boolean value indicates if the given actions should be shuffled.
    // The action array can be configured with any number of actions
    // POS_YAW：Shake head
    // MOUTH：Open mouth
    // BLINK：Blink

    Alert.alert('Done');
  };

  // const getLicense = () => {
  //   function getMoviesFromApiAsync() {
  //     return fetch('https://api.advance.ai/openapi/liveness/v1/auth-license', {
  //       method: 'POST',
  //       headers: {
  //         Accept: 'application/json',
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify({
  //         licenseEffectiveSeconds: '36000',
  //       }),
  //     })
  //       .then(response => response.json())
  //       .then(responseJson => {
  //         return responseJson.movies;
  //       })
  //       .catch(error => {
  //         console.error(error);
  //       });
  //   }

  //   getMoviesFromApiAsync();
  // };

  const getLicense = async () => {
    const reqParams = {
      licenseEffectiveSeconds: 600,
      // applicationId: APP_ID,
    };
    // const url = Config.liveness.getLicense;
    const url = "https://api.advance.ai/openapi/liveness/v1/auth-license";

    try {
      const response = await fetch(url, {
        body: JSON.stringify(reqParams),
        headers: {
          'Content-Type': 'application/json',
          'X-Advai-Key': "d7da2a9857fd1693",
        },
        method: 'POST',
      });
      const json = await response.json();
      // setLicense(json.data.license);
      // debugLog(TAG, 'getLicense success:', json);
      console.log(json)
      Alert.alert("Success", JSON.stringify(json) )
    } catch (error) {
      // debugLog(TAG, 'getLicense error:', error);
      console.log(error)
    } 
    // finally {
    //   setLoading(false);
    // }
  };

  return (
    <View>
      <Text>
        This is bryan sample application using IQA provided custom for
        specific client by Delivery Team
      </Text>
      <Text>---</Text>
      <Text>
        PS: This is not official, the intention of this is just to show how to
        integrate IQA into React Native Application
      </Text>
      <Text>---</Text>
      <Text>v1.2.0</Text>
      {/* <Button onPress={getLicense} title="getLicense!" color="#0099AA" /> */}
      <Button onPress={showLiveness} title="IQA!" color="#0099AA" />
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  buttonContainer: {
    margin: 20,
  },
  multiButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
